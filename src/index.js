import './style';
import App from './components';
import MasterProvider from './providers/master.provider';

function Index() {
	return (
		<MasterProvider>
			<App />
		</MasterProvider>
	)
}

export default Index;