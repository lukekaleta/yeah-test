const DEV = {
	END_POINT: 'http://terminal101.adler.local:5002/api/v1',
	SOCKET_POINT: 'http://terminal101.adler.local:5002'
}

const IS_DEV = process.env.NODE_ENV === "development";

const CONF = {
	END_POINT: '/api/v1',
	SOCKET_POINT: '',
	...(IS_DEV ? DEV : {})
}

const config = {
	END: CONF.END_POINT,
	SOCKET: CONF.SOCKET_POINT
}

export default config;