import { h, createContext } from "preact";
import { useState, StateUpdater } from "preact/hooks";

type Pagination = {
  start: number;
  limit: number;
};

type Context = {
  get: (params: Pagination) => void;
  data: any | any[];
};

export const ApiContext = createContext<Context>(null as any);

const ApiProvider = ({ children }: any) => {
  const [data, setData] = useState([]);

  function _uriQuery(params: Pagination) {
    let arr = [];
    for (let key in params) {
      if (params[key]) {
        arr.push(`${key}=${params[key]}`);
      }
    }
    if (arr.length > 0) {
      return "?" + arr.join("&");
    } else {
      return "";
    }
  }

  function makeUrl(params: Pagination) {
    const url = "https://jsonplaceholder.typicode.com/photos";
    const uriQuery = params ? _uriQuery(params) : "";
    return url + uriQuery;
  }

  async function get(params: Pagination) {
    const url = makeUrl(params);
    const fetchConfig: any = {
      method: "GET",
      headers: {},
    };
    const response = await fetch(url, fetchConfig);
    return response.status === 200 ? setData(response.json()) : false;
  }

  return (
    <ApiContext.Provider
      value={{
        get,
        data,
      }}
    >
      {children}
    </ApiContext.Provider>
  );
};

export default ApiProvider;
