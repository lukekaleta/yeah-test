import { h } from "preact";

import AppProvider from "./app.provider";
import ApiProvider from "./api.provider";

function combineProviders(providerArr: any[]) {
  function createTree(index, children) {
    const providerNode = providerArr[index];
    const component = providerNode;
    if (index === providerArr.length - 1) {
      return h(component, { children });
    } else {
      return h(component, {}, createTree(++index, children));
    }
  }

  return ({ children }) => {
    return createTree(0, children);
  };
}

const MasterProvider = combineProviders([AppProvider, ApiProvider]);

export default MasterProvider;
