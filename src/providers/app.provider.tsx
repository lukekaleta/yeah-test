import { h, createContext } from "preact";
import { useState, StateUpdater } from "preact/hooks";

type Context = {
  isLoading: boolean;
  setLoading: StateUpdater<boolean>;
  dark: boolean;
  setDark: StateUpdater<boolean>;
};

export const AppContext = createContext<Context>(null as any);

const AppProvider = ({ children }: any) => {
  const [isLoading, setLoading] = useState(false);
  const [dark, setDark] = useState(false);

  return (
    <AppContext.Provider
      value={{
        isLoading,
        setLoading,
        dark,
        setDark,
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default AppProvider;
