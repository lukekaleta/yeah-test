import { h, VNode } from "preact";
import cn from "../../lib/cn";

const styles = require("./styles.css");

type Props = {
  children: string | VNode | VNode[];
  type?: "default" | "primary" | "outline";
  variant?: "contained" | "text" | "outline";
  onClick?: (e) => void;
  className?: string;
  loading?: boolean;
  disabled?: boolean;
};

function Button({
  children,
  type = "default",
  variant = "contained",
  className,
  onClick,
  loading = false,
  disabled = false,
}: Props) {
  return (
    <button
      className={cn(
        styles.button,
        styles[`button__${variant}__${type}`],
        {
          [styles.button__disabled]: disabled || loading,
        },
        className
      )}
      onClick={onClick}
      disabled={disabled || loading}
    >
      <span>{children}</span>
    </button>
  );
}

export default Button;
