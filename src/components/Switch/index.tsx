import { h } from "preact";
import cn from "../../lib/cn";

const styles = require("./styles.css");

type Props = {
  onChecked?: (checked: boolean) => void;
  className?: string;
  checked?: boolean;
};

function Switch({ checked, onChecked, className }: Props) {
  return (
    <div>
      <label className={cn(styles.switch, className)}>
        <input
          type="checkbox"
          checked={checked}
          onInput={(e) => onChecked(e.currentTarget.checked)}
        />
        <span className={cn(styles.slider, styles.round)}></span>
      </label>
    </div>
  );
}

export default Switch;
