import { h } from "preact";

import Root from "../routes/root";

function index() {
  return (
    <div>
      <Root />
    </div>
  );
}

export default index;
