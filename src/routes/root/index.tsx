import { h } from "preact";
import { useContext, useEffect, useState } from "preact/hooks";
import cn from "../../lib/cn";

// PROVIDERS
import { AppContext } from "../../providers/app.provider";
import { ApiContext } from "../../providers/api.provider";

// COMPONENTS
import Button from "../../components/Button";
import Switch from "../../components/Switch";

const styles = require("./styles.css");

function Root() {
  const { dark, isLoading, setDark, setLoading } = useContext(AppContext);
  const { get, data } = useContext(ApiContext);
  const [start, setStart] = useState(0);
  const [limit, setLimit] = useState(10);

  async function refresh() {
    await get({ start, limit });
  }

  useEffect(() => {
    refresh();
  }, []);

  async function handleLoadMore() {
    await get({ start, limit });
  }

  return (
    <div id="app" className={dark ? styles.dark : styles.light}>
      <div
        className={cn(
          styles.content,
          dark ? styles.dark_content : styles.light_content,
          "p-2"
        )}
      >
        <div className="f f-jc-end">
          <Switch onChecked={setDark} />
        </div>
        <div className="f f-jc-center">
          <h1>Infinite list</h1>
        </div>
        <div className="f f-jc-center pt-4">
          <Button
            type="primary"
            variant="contained"
            loading={isLoading}
            onClick={handleLoadMore}
          >
            Load more
          </Button>
        </div>
      </div>
    </div>
  );
}

export default Root;
