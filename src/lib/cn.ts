// Class Name builder

type classArgument = string | { [className: string]: boolean }

function cn(...args: classArgument[]) {
	const arr = [];
	for (const e of args) {
		if (e) {
			if (typeof e === 'object') {
				for (const [ key, value ] of Object.entries(e)) {
					if (value) {
						arr.push(key);
					}
				}
			}
			if (typeof e === "string") {
				arr.push(e);
			}
		}
	}
	return arr.join(' ');
}

export default cn;